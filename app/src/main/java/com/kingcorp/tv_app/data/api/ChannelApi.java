package com.kingcorp.tv_app.data.api;

import com.kingcorp.tv_app.domain.entity.Channels;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ChannelApi {
    @GET("/api.php")
    Call<Channels> loadChannelsList();
}
