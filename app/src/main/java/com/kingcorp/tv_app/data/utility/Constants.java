package com.kingcorp.tv_app.data.utility;

public class Constants {
    public static final String APP_PACKAGE = "com.kingcorp.tv_app";
    public static final String CHANNEL_LIST_EXTRA_KEY = "com.tv_app.get_channel_list";
    public static final String CHANNEL_INDEX_EXTRA_KEY = "com.tv_app.get_channel_index";
    public static final String AD_STATE_KEY = "com.tv_app.get_ad_state";
    public static final String CHANNEL_RECYCLER_STATE_KEY = "com.tv_app.get_recycler_state";
    public static final String CHANNEL_LISTENER_KEY = "com.tv_app.get_listener";
    public static final String INAPP_TURN_OFF_ADS = "turn_off_ads";

}
