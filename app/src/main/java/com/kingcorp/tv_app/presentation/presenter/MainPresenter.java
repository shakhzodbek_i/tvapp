package com.kingcorp.tv_app.presentation.presenter;


import com.kingcorp.tv_app.presentation.listeners.MainActivityListener;

public interface MainPresenter{
    void loadChannels();

    MainActivityListener getListener();

    MainPresenterImpl.UpdateListener getUpdateListener();
}
