package com.kingcorp.tv_app.presentation.view;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingcorp.tv_app.R;
import com.kingcorp.tv_app.data.utility.Constants;
import com.kingcorp.tv_app.domain.entity.Channel;
import com.kingcorp.tv_app.presentation.adapters.ChannelsAdapter;
import com.kingcorp.tv_app.presentation.listeners.MainActivityListener;

import java.util.ArrayList;

public class ChannelFragment extends Fragment {
    private RecyclerView mChannelsRecyclerView;
    private Parcelable mRecyclerViewState;
    private ChannelsAdapter mAdapter;

    private ArrayList<Channel> mChannels;
    private MainActivityListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.channel_list, container, false);

        mChannels = getArguments().getParcelableArrayList(Constants.CHANNEL_LIST_EXTRA_KEY);
        mListener = getArguments().getParcelable(Constants.CHANNEL_LISTENER_KEY);

        mChannelsRecyclerView = view.findViewById(R.id.channel_list);
        mChannelsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mAdapter = new ChannelsAdapter(mChannels, mListener);
        mChannelsRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mRecyclerViewState = mChannelsRecyclerView.getLayoutManager().onSaveInstanceState();
        outState.putParcelable(Constants.CHANNEL_RECYCLER_STATE_KEY, mRecyclerViewState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mRecyclerViewState = savedInstanceState.getParcelable(Constants.CHANNEL_RECYCLER_STATE_KEY);

            if (mRecyclerViewState != null)
                mChannelsRecyclerView.getLayoutManager().onRestoreInstanceState(mRecyclerViewState);
        }
    }

    public static ChannelFragment newInstance(ArrayList<Channel> channels, MainActivityListener listener) {
        ChannelFragment fragment = new ChannelFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.CHANNEL_LIST_EXTRA_KEY, channels);
        bundle.putParcelable(Constants.CHANNEL_LISTENER_KEY, listener);
        fragment.setArguments(bundle);
        return fragment;
    }

}
