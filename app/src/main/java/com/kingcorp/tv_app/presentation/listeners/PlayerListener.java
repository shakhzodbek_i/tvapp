package com.kingcorp.tv_app.presentation.listeners;

import android.view.View;

public interface PlayerListener {
    void onControlButtonClick(View view);

    void onSurfaceClick(View view);
}
