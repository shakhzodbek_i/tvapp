package com.kingcorp.tv_app.presentation.listeners;

import android.os.Parcel;
import android.os.Parcelable;

import com.kingcorp.tv_app.domain.entity.Channel;

public interface MainActivityListener extends Parcelable {
    void onChannelClick(Channel currentChannel);

    @Override
    int describeContents();

    @Override
    void writeToParcel(Parcel parcel, int i);
}
