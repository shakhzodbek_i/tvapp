package com.kingcorp.tv_app.presentation.presenter;

import android.os.Parcel;
import android.support.annotation.NonNull;

import com.android.billingclient.api.Purchase;
import com.kingcorp.tv_app.data.billing.BillingManager;
import com.kingcorp.tv_app.data.utility.Constants;
import com.kingcorp.tv_app.data.utility.SharedPreferencesHelper;
import com.kingcorp.tv_app.domain.entity.Channel;
import com.kingcorp.tv_app.domain.entity.Channels;
import com.kingcorp.tv_app.domain.repository.ChannelRepository;
import com.kingcorp.tv_app.presentation.listeners.MainActivityListener;
import com.kingcorp.tv_app.presentation.view.MainView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenterImpl implements MainPresenter {

    private final MainView mView;
    private final ChannelRepository mRepository;
    private final SharedPreferencesHelper mSharedPreferencesHelper;
    private List<Channel> mChannelsList;

    private MainActivityListener mListener = new MainActivityListener() {
        @Override
        public void onChannelClick(Channel currentChannel) {
            mView.openChannel(currentChannel, new ArrayList<>(mChannelsList));
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {

        }
    };

    public MainPresenterImpl(MainView mView, ChannelRepository repository, SharedPreferencesHelper sharedPreferencesHelper) {
        this.mView = mView;
        this.mRepository = repository;
        this.mSharedPreferencesHelper = sharedPreferencesHelper;
    }

    @Override
    public void loadChannels() {
        mView.showProgressBar();

        mRepository.loadChannels()
                .enqueue(new Callback<Channels>() {
                    @Override
                    public void onResponse(@NonNull Call<Channels> call, @NonNull Response<Channels> response) {
                        if (response.body() != null) {
                            mChannelsList = response.body().channels;
                            mView.showChannels(mChannelsList);
                            mView.hideProgressBar();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Channels> call, @NonNull Throwable t) {
                        mView.showNoInternetConnection();
                    }
                });
    }

    public class UpdateListener implements BillingManager.BillingUpdatesListener {
        @Override
        public void onBillingClientSetupFinished() {
            mView.initAdsOffPurchase();
        }

        @Override
        public void onPurchasesUpdated(List<Purchase> purchaseList) {
            for (Purchase purchase : purchaseList) {
                if (Constants.INAPP_TURN_OFF_ADS.equals(purchase.getSku())) {
                    mSharedPreferencesHelper.saveBoolean(false, Constants.AD_STATE_KEY);
                    mView.refreshAdsState();
                }
            }
        }
    }

    @Override
    public MainActivityListener getListener() {
        return mListener;
    }

    @Override
    public UpdateListener getUpdateListener() {
        return new UpdateListener();
    }
}
