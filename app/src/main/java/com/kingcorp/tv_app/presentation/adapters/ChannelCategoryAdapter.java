package com.kingcorp.tv_app.presentation.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class ChannelCategoryAdapter extends FragmentPagerAdapter {

    private List<Fragment> mFragmentList;
    private List<String> mCategoryList;

    public ChannelCategoryAdapter(FragmentManager fm, List<Fragment> fragments, List<String> categories) {
        super(fm);

        this.mFragmentList = fragments;
        this.mCategoryList = categories;
    }

    @Override
    public Fragment getItem(int i) {
        return mFragmentList.get(i);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (mCategoryList.size() != 0) {
            return mCategoryList.get(position);
        }
        else
            return "No title";
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
