package com.kingcorp.tv_app.presentation.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.kingcorp.tv_app.R;
import com.kingcorp.tv_app.data.billing.BillingManager;
import com.kingcorp.tv_app.data.utility.Constants;
import com.kingcorp.tv_app.data.utility.SharedPreferencesHelper;
import com.kingcorp.tv_app.domain.entity.Channel;
import com.kingcorp.tv_app.domain.repository.ChannelRepository;
import com.kingcorp.tv_app.presentation.adapters.ChannelCategoryAdapter;
import com.kingcorp.tv_app.presentation.listeners.MainActivityListener;
import com.kingcorp.tv_app.presentation.presenter.MainPresenter;
import com.kingcorp.tv_app.presentation.presenter.MainPresenterImpl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

import static com.kingcorp.tv_app.data.billing.BillingManager.BILLING_MANAGER_NOT_INITIALIZED;

public class MainActivity extends AppCompatActivity
        implements MainView, NavigationView.OnNavigationItemSelectedListener {

    private ViewPager mChannelPager;
    private TabLayout mTabLayout;
    private ChannelCategoryAdapter mPagerAdapter;
    private LinearLayout mNoConnectionView;
    private Button mRestartBtn;
    private MainPresenter mPresenter;
    private ProgressBar mProgressBar;
    private Toolbar mToolBar;
    private NavigationView mNavigationView;

    private AdView mAdViewBanner;
    private InterstitialAd mInterstitialAd;

    private BillingManager mBillingManager;
    private boolean mIsAdOn;

    @Inject
    ChannelRepository mRepository;

    @Inject
    SharedPreferencesHelper mSharedPreferencesHelper;
    private MainActivityListener mListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);

        mIsAdOn = mSharedPreferencesHelper.getBoolean(Constants.AD_STATE_KEY, true);

        if (mIsAdOn)
            setContentView(R.layout.activity_main);
        else
            setContentView(R.layout.activity_main_noads);

        initView();

        mPresenter = new MainPresenterImpl(this, mRepository, mSharedPreferencesHelper);
        mListener = mPresenter.getListener();
        mPresenter.loadChannels();
    }

    @Override
    public void showChannels(List<Channel> channels) {
        ArrayList<Fragment> fragments = new ArrayList<>();
        ArrayList<Channel> tempList;
        ArrayList<String> categories = new ArrayList<>();

        for (Channel channel : channels){
            tempList = new ArrayList<>();
            if (!categories.contains(channel.getCategory())) {
                categories.add(channel.getCategory());
                for (int i = 0; i < channels.size(); i++) {
                    if (channel.getCategory().equals(channels.get(i).getCategory()))
                        tempList.add(channels.get(i));
                }

                fragments.add(ChannelFragment.newInstance(tempList, mListener));
            }
        }
        mPagerAdapter = new ChannelCategoryAdapter(getSupportFragmentManager(), fragments, categories);

        mChannelPager.setAdapter(mPagerAdapter);
    }

    @Override
    public void openChannel(Channel channel, ArrayList<Channel> channelList) {
        Intent intent = new Intent(this, PlayerActivity.class);
        intent.putParcelableArrayListExtra(Constants.CHANNEL_LIST_EXTRA_KEY, channelList);
        intent.putExtra(Constants.CHANNEL_INDEX_EXTRA_KEY, channelList.indexOf(channel));
        intent.putExtra(Constants.AD_STATE_KEY, mIsAdOn);
        startActivity(intent);
    }

    @Override
    public void showProgressBar() {
        mChannelPager.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mChannelPager.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    private void initView() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mToolBar = findViewById(R.id.toolbar);
        mToolBar.setTitle("");
        setSupportActionBar(mToolBar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolBar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        if (!mIsAdOn)
            mNavigationView.getMenu().findItem(R.id.nav_off_ads).setVisible(false);

        setViewPager();

        mProgressBar = findViewById(R.id.main_progress);
        mNoConnectionView = findViewById(R.id.no_connection);
        mRestartBtn = findViewById(R.id.restart_btn);
        mRestartBtn.setOnClickListener(view -> {
            mNoConnectionView.setVisibility(View.GONE);
            mPresenter.loadChannels();
        });

        initAd();
    }

    private void setViewPager() {
        mChannelPager = findViewById(R.id.channel_pager);

        mTabLayout = findViewById(R.id.tab_layout);

        mTabLayout.setupWithViewPager(mChannelPager);
    }

    private void initAd() {
        if (mIsAdOn) {
            mAdViewBanner = findViewById(R.id.adView);
            mAdViewBanner.loadAd(new AdRequest.Builder().build());
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.admob_inter_unit_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener(){
                @Override
                public void onAdClosed() {
                    mInterstitialAd.loadAd(new AdRequest.Builder().build());
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_off_ads:
                onAdOffButtonPressed();
                break;
            case R.id.nav_rate:
                Uri appUri = Uri.parse("market://details?id="+Constants.APP_PACKAGE);
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, appUri);
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        (Build.VERSION.SDK_INT >= 21
                                ? Intent.FLAG_ACTIVITY_NEW_DOCUMENT
                                : Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET) |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                startActivity(goToMarket);
                break;
            case R.id.nav_policy:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://sites.google.com/view/iptv-hdtv"));
                startActivity(browserIntent);
                break;

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onAdOffButtonPressed() {
        mBillingManager = new BillingManager(this, mPresenter.getUpdateListener());
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showAd();
    }

    private void showAd(){
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("AD_TAG", "The interstitial wasn't loaded yet.");
        }
    }

    @Override
    public void initAdsOffPurchase() {
        if (mBillingManager != null
                && mBillingManager.getBillingClientResponseCode() > BILLING_MANAGER_NOT_INITIALIZED) {
            mBillingManager.initiatePurchaseFlow(Constants.INAPP_TURN_OFF_ADS, BillingClient.SkuType.INAPP);
        }
    }

    public void refreshAdsState() {
        if (mAdViewBanner != null && mInterstitialAd != null){
            recreate();
        }
    }

    @Override
    public void showNoInternetConnection() {
        mProgressBar.setVisibility(View.GONE);
        mChannelPager.setVisibility(View.INVISIBLE);
        mNoConnectionView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBillingManager != null)
            mBillingManager.destroy();
    }

}
